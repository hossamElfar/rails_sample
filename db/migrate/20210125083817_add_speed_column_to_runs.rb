class AddSpeedColumnToRuns < ActiveRecord::Migration[6.1]
  def change
    add_column :runs, :speed, :float, default: 0.0
  end
end
