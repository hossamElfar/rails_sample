class AddLocationColumnToRun < ActiveRecord::Migration[6.1]
  def change
    add_column :runs, :location, :string
  end
end
