require 'rails_helper'

RSpec.describe "Users::Sessions", type: :request do
  describe "POST /users/sign_in" do
    let(:email) { Faker::Internet.email }
    let(:password) { SecureRandom.uuid }
    let(:params) do
      {
        user: {
          email: email,
          password: password
        }
      }
    end
    let(:parsed_response) { JSON.parse(response.body, symbolize_names: true) }

    context 'when user enter valid email and password' do
      let!(:user) { create :user, email: email, password: password }
      let!(:expected_response) do
        {
          id: user.id,
          name: user.name,
          email: user.email,
          role: user.role
        }
      end

      it 'should login successfuly and return the jwt token' do
        post user_session_path, params: params, as: :json

        expect(response).to have_http_status(201)
        expect(response.headers['Authorization']).not_to be_blank
      end

      it 'should return the user basic info' do
        post user_session_path, params: params, as: :json

        expect(response).to have_http_status(201)
        expect(parsed_response).to eq expected_response
      end
    end

    context 'when user enters invalid email or password' do
      let(:email) { 'invalid@email.com' }
      let(:password) { 'invalidpassword' }

      it 'should not login successfuly' do
        post user_session_path, params: params, as: :json

        expect(response).to have_http_status(401)
        expect(response.headers['Authorization']).to be_blank
      end
    end
  end

  describe "DELETE /users/sign_out" do
    let(:user) { create :user }
    let(:headers) { {} }

    context 'when user provides valid auth headers' do
      let(:headers) { Devise::JWT::TestHelpers.auth_headers({}, user) }

      it 'should sign out successfuly' do
        delete destroy_user_session_path, headers: headers, as: :json

        expect(response).to have_http_status(200)
      end

      it 'should add the token to the JwtDenylist' do
        expect do
          delete destroy_user_session_path, headers: headers, as: :json
        end.to change { JwtDenylist.count }.by(1)
      end
    end

    context 'when user provides wrong authorization token' do
      it 'should not change the JwtDenylist' do
        expect do
          delete destroy_user_session_path, headers: headers, as: :json
        end.to change { JwtDenylist.count }.by(0)
      end
    end
  end
end
