# typed: false
require "rails_helper"

RSpec.describe FiltersParser do
  describe '.parse' do
    test_cases =
      [
        {
          input: "(distance eq 1)",
          output: "( distance_m =  '1'  )"
        },
        {
          input: "(distance ne 1)",
          output: "( distance_m !=  '1'  )"
        },
        {
          input: "(distance gt 1)",
          output: "( distance_m >  '1'  )"
        },
        {
          input: "(distance lt 1)",
          output: "( distance_m <  '1'  )"
        },
        {
          input: "(time eq 1)",
          output: "( time_sec =  '1'  )"
        },
        {
          input: "(time ne 1)",
          output: "( time_sec !=  '1'  )"
        },
        {
          input: "(time gt 1)",
          output: "( time_sec >  '1'  )"
        },
        {
          input: "(time lt 1)",
          output: "( time_sec <  '1'  )"
        },
        {
          input: "(time lt 1) AND (distance gt 4)",
          output: "( time_sec <  '1'  ) AND ( distance_m >  '4'  )"
        },
        {
          input: "(time lt 1) AND ((distance gt 9) OR (distance lt 2))",
          output: "( time_sec <  '1'  ) AND (( distance_m >  '9'  ) OR ( distance_m <  '2'  ) )"
        },
        {
          input: "((time gt 3) AND (distance lt 50)) OR ((distance gt 9) OR (distance lt 2))",
          output: "(( time_sec >  '3'  ) AND ( distance_m <  '50'  ) ) OR (( distance_m >  '9'  ) OR ( distance_m <  '2'  ) )"
        },
        {
          input: "(location eq France)",
          output: "( location =  'France'  )"
        },
        {
          input: "location eq France",
          output: "location =  'France' "
        },
        {
          input: "location eq France AND distance gt 8",
          output: "location =  'France'  AND distance_m >  '8' "
        },
        {
          input: "location eq France AND distance gt 8 OR time gt 7",
          output: "location =  'France'  AND distance_m >  '8'  OR time_sec >  '7' "
        },
        {
          input: "location eq France AND distance gt 8 OR time gt 7 AND ((distance gt 8) OR (time < 90))",
          output: "location =  'France'  AND distance_m >  '8'  OR time_sec >  '7'  AND (( distance_m >  '8'  ) OR ( time_sec <  '90'  ) )"
        }
      ]

    context "when filters provided" do
      test_cases.each do |test_case|
        it "should return the correct parsed filter" do
          expect(FiltersParser.parse(test_case[:input])).to eq test_case[:output]
        end
      end
    end
  end
end
