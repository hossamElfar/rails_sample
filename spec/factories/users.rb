FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password { SecureRandom.uuid }

    trait :admin do
      role { :admin }
    end
    trait :regular_user do
      role { :user }
    end
    trait :user_manager do
      role { :user_manager }
    end
  end
end
