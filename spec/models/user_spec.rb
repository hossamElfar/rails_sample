require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create :user }

  describe '.jwt_payload' do
    let(:expected) do
      {
        email: user.email,
        role: user.role
      }
    end
    it 'should return the correct data' do
      expect(user.jwt_payload).to eq expected
    end
  end

  describe '.admin!' do
    before do
      user.user!
    end

    it 'should make the user admin' do
      user.admin!

      expect(user.role.to_sym).to eq :admin
    end
  end

  describe '.user!' do
    before do
      user.admin!
    end

    it 'should make the user a regular user' do
      user.user!

      expect(user.role.to_sym).to eq :user
    end
  end

  describe '.user_manager!' do
    before do
      user.user!
    end

    it 'should make the user a user manager' do
      user.user_manager!

      expect(user.role.to_sym).to eq :user_manager
    end
  end

  describe '.admin?' do
    context 'when the user is admin' do
      before do
        user.admin!
      end

      it 'should return true' do
        expect(user.admin?).to be_truthy
      end
    end

    context 'when the user is not admin' do
      before do
        user.user!
      end

      it 'should return false' do
        expect(user.admin?).not_to be_truthy
      end
    end
  end

  describe '.user?' do
    context 'when the user is regular user' do
      before do
        user.user!
      end

      it 'should return true' do
        expect(user.user?).to be_truthy
      end
    end

    context 'when the user is not a regular user' do
      before do
        user.admin!
      end

      it 'should return false' do
        expect(user.user?).not_to be_truthy
      end
    end
  end

  describe '.user_manager?' do
    context 'when the user is user manager' do
      before do
        user.user_manager!
      end

      it 'should return true' do
        expect(user.user_manager?).to be_truthy
      end
    end

    context 'when the user is not a user manager' do
      before do
        user.user!
      end

      it 'should return false' do
        expect(user.user_manager?).not_to be_truthy
      end
    end
  end
end
