require 'rails_helper'

describe ApplicationPolicy do
  subject { described_class }
  let(:regular_user) { create :user, :regular_user }
  let(:other_regular_user) { create :user, :regular_user }
  let(:admin) { create :user, :admin }
  let(:user_manager) { create :user, :user_manager }

  permissions :create?, :destroy?, :update?, :index?, :show?, :new?, :edit? do
    it "denies access for user" do
      expect(subject).not_to permit(regular_user, other_regular_user)
    end
  end
end
