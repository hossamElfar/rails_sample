require 'rails_helper'

describe RunPolicy do
  subject { described_class }
  let(:regular_user) { create :user, :regular_user }
  let(:other_regular_user) { create :user, :regular_user }
  let(:admin) { create :user, :admin }
  let(:user_manager) { create :user, :user_manager }
  let(:run) { create :run, user: regular_user }

  permissions :create?, :destroy?, :update?, :edit?, :report?, :index?, :show? do
    it "denies access for user manager" do
      expect(subject).not_to permit(user_manager, run)
    end

    it "grants access for admin" do
      expect(subject).to permit(admin, run)
    end

    it "grants access for user" do
      expect(subject).to permit(regular_user, run)
    end
  end

  permissions :update?, :edit?, :show? do
    it "grant access for regular user" do
      expect(subject).to permit(regular_user, run)
    end

    it "denies access for another user" do
      expect(subject).not_to permit(other_regular_user, run)
    end
  end
end
