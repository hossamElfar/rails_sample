class UsersController < ApplicationController
  include Authenticatable
  include Pundit

  before_action :authenticate!
  before_action :validate_role, only: [:create, :update]
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    authorize User

    page = (params[:page] || 1)
    per_page = (params[:per_page] || PER_PAGE)

    @users = User.search_records(filters).page(page).per(per_page)

    render json: {
      users: @users.map { |user| user.decorate.to_json },
      meta: collection_meta_info(@users)
    }, status: :ok
  end

  # GET /users/1
  def show
    authorize @user

    render json: @user.decorate.to_json
  end

  # POST /users
  def create
    authorize User

    @user = User.new(user_params)

    if @user.save
      render json: @user.decorate.to_json, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    authorize @user

    if @user.update(user_params)
      render json: @user.decorate.to_json
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    authorize @user

    @user.destroy
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    permitted_params = [:email, :password, :name, :role]
    params.require(:user).permit(permitted_params).compact
  end

  def validate_role
    return if current_user.admin?

    role = user_params[:role]
    return if role.blank?

    raise Pundit::NotAuthorizedError, 'not allowed action' if current_user.user?

    raise Pundit::NotAuthorizedError, 'not allowed action' if current_user.user_manager? && role.eql?('admin')
  end

  def filters
    {
      conditions: {
        dynamic_filters: FiltersParser.parse(params[:filters])
      }
    }
  end
end
