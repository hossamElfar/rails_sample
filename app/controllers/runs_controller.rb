class RunsController < ApplicationController
  include Authenticatable
  include Pundit

  before_action :authenticate!
  before_action :prepare_user
  before_action :set_run, only: [:show, :update, :destroy]

  # GET /runs
  def index
    authorize Run

    page = (params[:page] || 1)
    per_page = (params[:per_page] || PER_PAGE)

    @runs = Run.search_records(filters).page(page).per(per_page)

    render json: {
      runs: @runs.map { |run| run.decorate.to_json },
      meta: collection_meta_info(@runs)
    }, status: :ok
  end

  # GET /runs/1
  def show
    authorize @run

    render json: @run.decorate.to_json
  end

  # POST /runs
  def create
    authorize Run

    @run = @user.runs.build(run_params)

    if @run.save
      render json: @run.decorate.to_json, status: :created
    else
      render json: @run.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /runs/1
  def update
    authorize @run

    if @run.update(run_params)
      render json: @run.decorate.to_json
    else
      render json: @run.errors, status: :unprocessable_entity
    end
  end

  # DELETE /runs/1
  def destroy
    authorize @run

    @run.destroy!
  end

  # GET /runs/report
  def report
    authorize Run

    page = (params[:page] || 1)
    per_page = (params[:per_page] || PER_PAGE)

    weeks = Run.report(filters).page(page).per(per_page)

    render json: {
      weeks: weeks,
      meta: collection_meta_info(weeks)
    }, status: :ok
  end

  private

  def set_run
    @run = @user.runs.find(params[:id])
  end

  def run_params
    permitted_params = [:lat, :lng, :date, :time_sec, :distance_m, :location]
    permitted_params << :user_id if current_user.admin?
    params.require(:run).permit(permitted_params).compact
  end

  def prepare_user
    if current_user.admin?
      @user = User.find params[:user_id]
    elsif current_user.user?
      raise Pundit::NotAuthorizedError, 'not allowed action' unless current_user.id.eql? params[:user_id].to_i

      @user = current_user
    else
      raise Pundit::NotAuthorizedError, 'not allowed action'
    end
  end

  def filters
    {
      conditions: {
        user_id: @user.id,
        dynamic_filters: FiltersParser.parse(params[:filters])
      }
    }
  end
end
