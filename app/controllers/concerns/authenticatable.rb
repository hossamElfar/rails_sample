# typed: false
# frozen_string_literal: true

module Authenticatable
  extend ActiveSupport::Concern

  def authenticate!
    return if user_signed_in?

    render json: { message: 'unauthorized' }, status: :unauthorized
  end
end
