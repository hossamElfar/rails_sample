# frozen_string_literal: true

class RunWeatherSetter
  include Sidekiq::Worker
  sidekiq_options queue: 'jogging::run-weather-setter'

  # args = {
  #   id: RUN_ID,
  # }
  def perform(run_id)
    run = Run.find_by id: run_id
    return unless run

    weather, status_code = Weatherapi.
                           find_weather_history_by(
                             lat: run.lat,
                             lng: run.lng,
                             date: run.date,
                             location: run.location
                           )
    return unless status_code.in?(200..299)

    run.update_columns(weather: weather)
  end
end
