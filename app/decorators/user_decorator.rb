class UserDecorator < Draper::Decorator
  delegate_all

  def to_json(*_args)
    {
      id: id,
      name: name,
      email: email,
      role: role
    }
  end
end
